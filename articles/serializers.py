from django.contrib.auth.models import User
from django.core.cache import cache

from rest_framework.serializers import ModelSerializer, SerializerMethodField
from .models import Article, Comment


class UserSerializers(ModelSerializer):
    class Meta:
        model = User
        exclude = ['password']

class UserMiniSerializers(ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'username']

class ArticleSerializers(ModelSerializer):
    related_user = UserMiniSerializers(read_only=True)
    authors = UserMiniSerializers(many=True, read_only=True)
    comments_count = SerializerMethodField()
    class Meta:
        model = Article
        fields = '__all__'

    def get_comments_count(self, obj):
        cache_key = f'{obj.id}comments_count'
        if cache.get(cache_key):
            return cache.get(cache_key)
        cache.set(cache_key, obj.comment_set.all().count(), 12 * 60)
        return cache.get(cache_key) 

class CommentSerializers(ModelSerializer):
    related_article = ArticleSerializers(read_only=True)
    related_user = UserMiniSerializers(read_only=True)
    class Meta:
        model = Comment
        fields = '__all__'